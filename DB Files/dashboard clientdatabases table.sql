-- MySQL dump 10.13  Distrib 5.7.12, for Win64 (x86_64)
--
-- Host: localhost    Database: dashboard
-- ------------------------------------------------------
-- Server version	5.6.21-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `clientdatabases`
--

DROP TABLE IF EXISTS `clientdatabases`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `clientdatabases` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `clientid` varchar(45) DEFAULT NULL,
  `monitorscreengroup` varchar(100) DEFAULT NULL,
  `shopname` varchar(45) DEFAULT NULL,
  `site` varchar(45) DEFAULT NULL,
  `url` varchar(200) DEFAULT NULL,
  `dbschema` varchar(45) DEFAULT NULL,
  `dbuser` varchar(45) DEFAULT NULL,
  `dbpassword` varchar(45) DEFAULT NULL,
  `replicationdburl` varchar(200) DEFAULT NULL,
  `replicationdbuser` varchar(45) DEFAULT NULL,
  `replicationdbpassword` varchar(45) DEFAULT NULL,
  `reportjcoerrorsfromdate` date DEFAULT NULL,
  `datecreated` datetime DEFAULT NULL,
  `createdby` varchar(100) DEFAULT NULL,
  `createdonhost` varchar(100) DEFAULT NULL,
  `datechanged` datetime DEFAULT NULL,
  `changedby` varchar(100) DEFAULT NULL,
  `changedonhost` varchar(100) DEFAULT NULL,
  `deletedflag` varchar(4) DEFAULT NULL,
  `datedeleted` datetime DEFAULT NULL,
  `deletedby` varchar(100) DEFAULT NULL,
  `deletedonhost` varchar(100) DEFAULT NULL,
  `lastupdate` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `clientdatabases`
--

LOCK TABLES `clientdatabases` WRITE;
/*!40000 ALTER TABLE `clientdatabases` DISABLE KEYS */;
INSERT INTO `clientdatabases` VALUES (2,'Cashbuild',NULL,'Marapong','S318','192.170.11.5:3306','offlinepos','dashboarduser','123','192.170.11.6:3307','root','mysqlroot',NULL,NULL,NULL,NULL,'2015-10-29 13:17:48','Marsia Simpson','Marsia-PC',NULL,NULL,NULL,NULL,'2018-08-17 09:30:47'),(3,'Cashbuild',NULL,'Modimolle','S310','192.170.1.2:3306','offlinepos','dashboarduser','123','192.170.1.8:3307','root','mysqlroot',NULL,NULL,NULL,NULL,'2015-10-29 13:17:56','Marsia Simpson','Marsia-PC',NULL,NULL,NULL,NULL,'2018-08-17 09:30:47'),(4,'Cashbuild',NULL,'Moruleng','S317','192.170.13.2:3306','offlinepos','dashboarduser','123','192.170.13.8:3307','root','mysqlroot',NULL,NULL,NULL,NULL,'2016-04-14 08:56:36','LinxAS Admin','Support4-PC',NULL,NULL,NULL,NULL,'2018-08-17 09:30:47'),(5,'Cashbuild',NULL,'Sebokeng','S314','192.170.2.2:3306','offlinepos','root','mysqlroot','192.170.2.8:3307','root','mysqlroot',NULL,NULL,NULL,NULL,'2015-10-29 13:18:11','Marsia Simpson','Marsia-PC',NULL,NULL,NULL,NULL,'2018-08-17 09:30:47'),(6,'Cashbuild',NULL,'Ezakheni','S305','192.170.0.5:3306','offlinepos','dashboarduser','123','192.170.0.6:3307','root','mysqlroot',NULL,NULL,NULL,NULL,'2015-10-29 13:18:26','Marsia Simpson','Marsia-PC',NULL,NULL,NULL,NULL,'2018-08-17 09:30:47'),(11,'ArthurFord',NULL,'Midlands Mall','58',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2018-08-22 07:11:58');
/*!40000 ALTER TABLE `clientdatabases` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping routines for database 'dashboard'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-08-22  9:12:47
