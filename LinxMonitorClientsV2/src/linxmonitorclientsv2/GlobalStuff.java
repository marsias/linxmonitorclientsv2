/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package linxmonitorclientsv2;

import java.sql.Connection;
import javafx.scene.image.Image;

/**
 *
 * @author marsia
 */
public class GlobalStuff
{
    
    private  String linxasClientName = new String();
    public final String getLinxasClientName(){return linxasClientName;}
    public  final void setLinxasClientName(String value){linxasClientName = value;}
    public String linxasClientNameProperty() {return linxasClientName;}

    private String monitorClientGroup = new String();
    public final String getMonitorClientGroup(){return monitorClientGroup;}
    public final void setMonitorClientGroup(String value){monitorClientGroup = value;}
    public String monitorClientGroupProperty() {return monitorClientGroup;}

    private String localDbName = new String();
    public final String getLocalDbName(){return localDbName;}
    public final void setLocalDbName(String value){localDbName = value;}
    public String localDbNameProperty() {return localDbName;}

    private String localDbUser = new String();
    public final String getLocalDbUser(){return localDbUser;}
    public final void setLocalDbUser(String value){localDbUser = value;}
    public String localDbUserProperty() {return localDbUser;}

    private String localDbPw = new String();
    public final String getLocalDbPw(){return localDbPw;}
    public final void setLocalDbPw(String value){localDbPw = value;}
    public String localDbPwProperty() {return localDbPw;}

    private String localDbURL = new String();
    public final String getLocalDbURL(){return localDbURL;}
    public final void setLocalDbURL(String value){localDbURL = value;}
    public String localDbURLProperty() {return localDbURL;}
    
    private String localDbHostname = new String();
    public final String getLocalDbHostname(){return localDbHostname;}
    public final void setLocalDbHostname(String value){localDbHostname = value;}
    public String localDbHostnameProperty() {return localDbHostname;}

    private String remoteDbName = new String();
    public final String getRemoteDbName(){return remoteDbName;}
    public final void setRemoteDbName(String value){remoteDbName = value;}
    public String remoteDbNameProperty() {return remoteDbName;}

    private String remoteDbUser = new String();
    public final String getRemoteDbUser(){return remoteDbUser;}
    public final void setRemoteDbUser(String value){remoteDbUser = value;}
    public String remoteDbUserProperty() {return remoteDbUser;}

    private String remoteDbPw = new String();
    public final String getRemoteDbPw(){return remoteDbPw;}
    public final void setRemoteDbPw(String value){remoteDbPw = value;}
    public String remoteDbPwProperty() {return remoteDbPw;}

    private String remoteDbURL = new String();
    public final String getRemoteDbURL(){return remoteDbURL;}
    public final void setRemoteDbURL(String value){remoteDbURL = value;}
    public String remoteDbURLProperty() {return remoteDbURL;}
    
    private String remoteDbHostname = new String();
    public final String getRemoteDbHostname(){return remoteDbHostname;}
    public final void setRemoteDbHostname(String value){remoteDbHostname = value;}
    public String remoteDbHostnameProperty() {return remoteDbHostname;}
    
    private Integer timeoutMilliseconds = 0;
    public final Integer getTimeoutMilliseconds(){return timeoutMilliseconds;}
    public final void setTimeoutMilliseconds(Integer value){timeoutMilliseconds = value;}
    public Integer timeoutMilliseconds() {return timeoutMilliseconds;}

    public final String homeDirectory = System.getProperty("user.home"); 
    public final String imagesDirectory = "C:\\LinxMonitorClientsV2\\images\\";
    public final String configfilesDirectory = "C:\\LinxMonitorClientsV2\\reports\\";
    
    public Connection localDbConnector = null;
    public Connection remoteDbConnector = null;

    public Image topImage;
 
}
