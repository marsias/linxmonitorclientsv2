/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package linxmonitorclientsv2;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import javafx.application.Application;
import static javafx.application.Application.launch;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.fxml.JavaFXBuilderFactory;
import javafx.geometry.Rectangle2D;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Screen;
import javafx.stage.Stage;

/**
 *
 * @author marsia
 */
public class LinxMonitorClientsV2 extends Application
{
    
    public org.apache.log4j.Logger cat = org.apache.log4j.Logger.getLogger("LinxMonitorClientsV2");
    
    private Stage stage;

    public static GlobalStuff globalStuff = new GlobalStuff();
    public static SQLPull sqlPull = new SQLPull();
    
    String[] sSplitArr;
    String screenId = "";

    // get database connection variables from file and initiate the global variables
    public Boolean initDBConnectParams()
    {

        System.out.println("initDBConnectParams");

        try
        {

            String str;

            globalStuff.setLinxasClientName("");
            globalStuff.setMonitorClientGroup("");
            globalStuff.setLocalDbName("");
            globalStuff.setLocalDbUser("");
            globalStuff.setLocalDbPw("");
            globalStuff.setLocalDbURL("");
            globalStuff.setRemoteDbName("");
            globalStuff.setRemoteDbUser("");
            globalStuff.setRemoteDbPw("");
            globalStuff.setRemoteDbURL("");
            globalStuff.setTimeoutMilliseconds(600000);
            

            BufferedReader in = new BufferedReader(new FileReader(globalStuff.configfilesDirectory + "LinxASSummaryReportA4.jasper"));

            if ((str = in.readLine()) != null)
            {
                sSplitArr = str.split("\\|");

                globalStuff.setLinxasClientName(sSplitArr[0]);
                globalStuff.setMonitorClientGroup(sSplitArr[1]);
                globalStuff.setLocalDbHostname(sSplitArr[2]);
                globalStuff.setLocalDbName(sSplitArr[3]);
                globalStuff.setLocalDbUser(sSplitArr[4]);
                globalStuff.setLocalDbPw(sSplitArr[5]);
                globalStuff.setRemoteDbHostname(sSplitArr[6]);
                globalStuff.setRemoteDbName(sSplitArr[7]);
                globalStuff.setRemoteDbUser(sSplitArr[8]);
                globalStuff.setRemoteDbPw(sSplitArr[9]);
                globalStuff.setTimeoutMilliseconds(Integer.parseInt(sSplitArr[10]));

                globalStuff.setLocalDbURL(globalStuff.getLocalDbHostname());
                globalStuff.setRemoteDbURL(globalStuff.getRemoteDbHostname());
                
                System.out.println("globalStuff.getLinxasClientName()="+globalStuff.getLinxasClientName());

                System.out.println("globalStuff.getLocalDbURL()=" + globalStuff.getLocalDbURL());
                System.out.println("globalStuff.getRemoteDbURL()=" + globalStuff.getRemoteDbURL());
            }
            else
            {
                dynamicStaticAlerts.dynamicErrorPopUpCreation("Configuration Error:", "Please contact Linx AS. Cannot find logon file.");
                System.err.println("initDBConnectParams error: Please contact Linx AS. Cannot find logon file.");
                cat.error("initDBConnectParams error: Please contact Linx AS. Cannot find logon file.");
                return false;
            }
            in.close();

        }
        catch (IOException e)
        {
            System.err.println("initDBConnectParams error: " + e);
            cat.error("initDBConnectParams error: " + e);
            dynamicStaticAlerts.dynamicErrorPopUpCreation("initDBConnectParams Error:", "Could not find database connection information. Please contact Linx AS to configure.");
            return false;
        }

        return true;

    }

    public void gotoStoreAlertsScreen() 
    {
        
        System.out.println("gotoStoreAlertsScreen "+globalStuff.getLinxasClientName());
        
        try 
        {

            StoreAlertsScreenController controller = (StoreAlertsScreenController) replaceSceneContent("StoreAlertsScreen.fxml");
            controller.setApp(this);

            System.gc();
        } 
        catch (Exception ex) 
        {
            System.err.println("gotoStoreAlertsScreen error "+ex.getMessage());
            cat.error("gotoStoreAlertsScreen error "+ex.getMessage());
        }
    }

    // to load a new screen / fxml file 
    private Initializable replaceSceneContent(String fxml) throws Exception 
    {
        
        System.out.println("replaceSceneContent fxml="+fxml);
        
        FXMLLoader loader = new FXMLLoader();
        InputStream in = LinxMonitorClientsV2.class.getResourceAsStream(fxml);
        loader.setBuilderFactory(new JavaFXBuilderFactory());
        loader.setLocation(LinxMonitorClientsV2.class.getResource(fxml));
        AnchorPane page = null;
        try 
        {
            page = (AnchorPane) loader.load(in);
        } 
        catch (Exception e)
        {
            System.out.println("replaceSceneContent exception fxml="+fxml+" e.getMessage()="+e.getMessage());
            e.printStackTrace();
        }
        finally 
        {
            in.close();
        } 
        
        Rectangle2D primScreenBounds = Screen.getPrimary().getVisualBounds();
        Double mainAnchorpaneHeight = primScreenBounds.getHeight();
        Double mainAnchorpaneWidth = primScreenBounds.getWidth();
        
        Scene scene = new Scene(page, mainAnchorpaneWidth, mainAnchorpaneHeight);
        stage.setScene(scene);
        stage.sizeToScene();
        
        System.out.println("End of replaceSceneContent");
        
        return (Initializable) loader.getController();
    }

    public static void main(String[] args)
    {
        launch(args);
    }
    
    @Override
    public void start(Stage primaryStage) throws Exception
    {

        System.out.println("start LinxMonitorClientsV2");

        primaryStage.setTitle("LinxAS Monitor Clients");
        
        initDBConnectParams();
        
        try 
        {
            stage = primaryStage;

            gotoStoreAlertsScreen();
            //gotoStartupScreen();
            
            primaryStage.show();
 
        } 
        catch (Exception ex) 
        {
            dynamicStaticAlerts.dynamicErrorPopUpCreation("StoreAlertsScreenController", "Error loading first screen " + ex);
            System.err.println("StoreAlertsScreenController - Error loading first screen " + ex);
            cat.error("StoreAlertsScreenController - Error loading first screen " + ex);
        }

    }
 
}
    
// *** EOF ***