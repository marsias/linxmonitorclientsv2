/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package linxmonitorclientsv2;


import com.mysql.cj.jdbc.MysqlDataSource;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import static linxmonitorclientsv2.LinxMonitorClientsV2.globalStuff;

/**
 *
 * @author marsia
 */
public class SQLPull
{
    
    public org.apache.log4j.Logger cat = org.apache.log4j.Logger.getLogger("SQLPull");

    Connection localDbConnector = null;
    Connection remoteConn = null;
    Statement remoteStmt = null;
    Statement localStatement = null;
    ResultSet dbresult = null;
    
    // *** local database ***
    
    // *** table clientdatabases 
    public List<String> pullClientDatabases()
    {

        System.out.println("pullClientDatabases");

        localStatement = null;
        dbresult = null;
        List<String> dbrows = new ArrayList<String>();

        String sqlString = "";

        try
        {
            
            if (!connectToLocalDb())
            {
                close();
                return dbrows;
            }

            sqlString = "SELECT " + 
                        "id, " +
                        "clientid, " +
                        "IFNULL(monitorscreengroup, 'All Stores'), " +
                        "shopname, " +
                        "site, " +
                        "url, " +
                        "dbschema, " +
                        "dbuser, " +
                        "dbpassword, " +
                        "replicationdburl, " +
                        "replicationdbuser, " +
                        "replicationdbpassword, " +
                        "reportjcoerrorsfromdate, " +
                        "datecreated, " +
                        "createdby, " +
                        "createdonhost, " +
                        "datechanged, " +
                        "changedby, " +
                        "changedonhost, " +
                        "deletedflag, " +
                        "datedeleted, " +
                        "deletedby, " +
                        "deletedonhost, " +
                        "lastupdate " +
                        "FROM " + globalStuff.getLocalDbName() + ".clientdatabases " +
                        "ORDER BY 2,3, 4 ASC";

            System.out.println("sqlString=" + sqlString);

            localStatement = localDbConnector.createStatement();
            dbresult = localStatement.executeQuery(sqlString);

            while (dbresult.next())
            {
                dbrows.add(dbresult.getString(1) + "|" + dbresult.getString(2) + "|" + dbresult.getString(3) + "|" + dbresult.getString(4) + "|" + dbresult.getString(5) + "|" +
                dbresult.getString(6) + "|" + dbresult.getString(7) + "|" + dbresult.getString(8) + "|" + dbresult.getString(9) + "|" + dbresult.getString(10) + "|" +
                dbresult.getString(11) + "|" + dbresult.getString(12) + "|" + dbresult.getString(13) + "|" + dbresult.getString(14) + "|" + dbresult.getString(15) + "|" +
                dbresult.getString(16) + "|" + dbresult.getString(17) + "|" + dbresult.getString(18) + "|" + dbresult.getString(19) + "|" + dbresult.getString(20) + "|" +
                dbresult.getString(21) + "|" + dbresult.getString(22) + "|" + dbresult.getString(23) + "|" + dbresult.getString(24) + "|");
            }

        }
        catch (Exception ex)
        {
            System.err.println("pullClientDatabases error - " + ex);
            cat.error("pullClientDatabases error - " + ex);
            dynamicStaticAlerts.dynamicErrorPopUpCreation("Error:", "pullClientDatabases - Error retrieving data.");
        }
        finally
        {
            close();
        }

        return dbrows;
    }

    public List<String> pullClientDatabasesForClientId(String clientid)
    {

        System.out.println("pullClientDatabasesForClientId");

        localStatement = null;
        dbresult = null;
        List<String> dbrows = new ArrayList<String>();

        String sqlString = "";

        try
        {
            
            if (!connectToLocalDb())
            {
                close();
                return dbrows;
            }

            sqlString = "SELECT " + 
                        "id, " +
                        "clientid, " +
                        "IFNULL(monitorscreengroup, 'All Stores'), " +
                        "shopname, " +
                        "site, " +
                        "url, " +
                        "dbschema, " +
                        "dbuser, " +
                        "dbpassword, " +
                        "replicationdburl, " +
                        "replicationdbuser, " +
                        "replicationdbpassword, " +
                        "reportjcoerrorsfromdate, " +
                        "datecreated, " +
                        "createdby, " +
                        "createdonhost, " +
                        "datechanged, " +
                        "changedby, " +
                        "changedonhost, " +
                        "deletedflag, " +
                        "datedeleted, " +
                        "deletedby, " +
                        "deletedonhost, " +
                        "lastupdate " +
                        "FROM " + globalStuff.getLocalDbName() + ".clientdatabases " +
                        "WHERE clientid = '"+clientid+"' " +
                        "ORDER BY 2,3,4 ASC";

            System.out.println("sqlString=" + sqlString);

            localStatement = localDbConnector.createStatement();
            dbresult = localStatement.executeQuery(sqlString);

            while (dbresult.next())
            {
                dbrows.add(dbresult.getString(1) + "|" + dbresult.getString(2) + "|" + dbresult.getString(3) + "|" + dbresult.getString(4) + "|" + dbresult.getString(5) + "|" +
                dbresult.getString(6) + "|" + dbresult.getString(7) + "|" + dbresult.getString(8) + "|" + dbresult.getString(9) + "|" + dbresult.getString(10) + "|" +
                dbresult.getString(11) + "|" + dbresult.getString(12) + "|" + dbresult.getString(13) + "|" + dbresult.getString(14) + "|" + dbresult.getString(15) + "|" +
                dbresult.getString(16) + "|" + dbresult.getString(17) + "|" + dbresult.getString(18) + "|" + dbresult.getString(19) + "|" + dbresult.getString(20) + "|" +
                dbresult.getString(21) + "|" + dbresult.getString(22) + "|" + dbresult.getString(23) + "|" + dbresult.getString(24) + "|");
            }

        }
        catch (Exception ex)
        {
            System.err.println("pullClientDatabasesForClientId error - " + ex);
            cat.error("pullClientDatabasesForClientId error - " + ex);
            dynamicStaticAlerts.dynamicErrorPopUpCreation("Error:", "pullClientDatabasesForClientId - Error retrieving data.");
        }
        finally
        {
            close();
        }

        return dbrows;
    }
    
    public List<String> pullClientDatabasesForClientIdAndMonitorGroup(String clientid, String monitorscreengroup)
    {

        System.out.println("pullClientDatabasesForClientIdAndMonitorGroup");

        localStatement = null;
        dbresult = null;
        List<String> dbrows = new ArrayList<String>();

        String sqlString = "";

        try
        {
            
            if (!connectToLocalDb())
            {
                close();
                return dbrows;
            }

            sqlString = "SELECT " + 
                        "id, " +
                        "clientid, " +
                        "IFNULL(monitorscreengroup, 'All Stores'), " +
                        "shopname, " +
                        "site, " +
                        "url, " +
                        "dbschema, " +
                        "dbuser, " +
                        "dbpassword, " +
                        "replicationdburl, " +
                        "replicationdbuser, " +
                        "replicationdbpassword, " +
                        "reportjcoerrorsfromdate, " +
                        "datecreated, " +
                        "createdby, " +
                        "createdonhost, " +
                        "datechanged, " +
                        "changedby, " +
                        "changedonhost, " +
                        "deletedflag, " +
                        "datedeleted, " +
                        "deletedby, " +
                        "deletedonhost, " +
                        "lastupdate " +
                        "FROM " + globalStuff.getLocalDbName() + ".clientdatabases " +
                        "WHERE clientid = '"+clientid+"' " +
                        "AND monitorscreengroup = '"+monitorscreengroup+"' " +
                        "ORDER BY 2,3,4 ASC";

            System.out.println("sqlString=" + sqlString);

            localStatement = localDbConnector.createStatement();
            dbresult = localStatement.executeQuery(sqlString);

            while (dbresult.next())
            {
                dbrows.add(dbresult.getString(1) + "|" + dbresult.getString(2) + "|" + dbresult.getString(3) + "|" + dbresult.getString(4) + "|" + dbresult.getString(5) + "|" +
                dbresult.getString(6) + "|" + dbresult.getString(7) + "|" + dbresult.getString(8) + "|" + dbresult.getString(9) + "|" + dbresult.getString(10) + "|" +
                dbresult.getString(11) + "|" + dbresult.getString(12) + "|" + dbresult.getString(13) + "|" + dbresult.getString(14) + "|" + dbresult.getString(15) + "|" +
                dbresult.getString(16) + "|" + dbresult.getString(17) + "|" + dbresult.getString(18) + "|" + dbresult.getString(19) + "|" + dbresult.getString(20) + "|" +
                dbresult.getString(21) + "|" + dbresult.getString(22) + "|" + dbresult.getString(23) + "|" + dbresult.getString(24) + "|");
            }

        }
        catch (Exception ex)
        {
            System.err.println("pullClientDatabasesForClientIdAndMonitorGroup error - " + ex);
            cat.error("pullClientDatabasesForClientIdAndMonitorGroup error - " + ex);
            dynamicStaticAlerts.dynamicErrorPopUpCreation("Error:", "pullClientDatabasesForClientIdAndMonitorGroup - Error retrieving data.");
        }
        finally
        {
            close();
        }

        return dbrows;
    }
    
    // *** remote database ***
    
    // *** table clientMonitorProcesses
    
    public List<String> pullClientclientMonitorProcessesForClientIdAndSite(String clientid, String site)
    {

        System.out.println("pullClientclientMonitorProcessesForClientIdAndSite");

        localStatement = null;
        dbresult = null;
        List<String> dbrows = new ArrayList<String>();

        String sqlString = "";

        try
        {
            
            if (!connectToRemoteDb())
            {
                close();
                return dbrows;
            }

            sqlString = "SELECT " + 
                        "clientId, " +
                        "site, " +
                        "storeName, " +
                        "DATE_FORMAT(lastUpdate, '%Y-%m-%d %H:%i:%S'), " +
                        "monitorJcoTransactionsInProcess, " +
                        "jcoTransactionsInProcessCount, " +
                        "DATE_FORMAT(jcoTransactionsLastDateProcessed, '%Y-%m-%d %H:%i:%S'), " +
                        "monitorJcoTransactionsBatched, " +
                        "jcoTransactionsBatchedCount, " +
                        "DATE_FORMAT(jcoTransactionsLastDateBatched, '%Y-%m-%d %H:%i:%S'), " +
                        "monitorJcoTransactionsNotProcessed, " +
                        "jcoTransactionsNotProcessedCount, " +
                        "DATE_FORMAT(jcoTransactionsLastDateNotProcessed, '%Y-%m-%d %H:%i:%S'), " +
                        "monitorJcoTransactionsErrors, " +
                        "jcoTransactionsErrorsCount, " +
                        "monitorArticleDownload, " +
                        "DATE_FORMAT(lastArticleDownloadTime, '%Y-%m-%d %H:%i:%S'), " +
                        "monitorPriceListsDownload, " +
                        "DATE_FORMAT(lastPriceListsDownloadTime, '%Y-%m-%d %H:%i:%S'), " +
                        "monitorScalesDownload, " +
                        "DATE_FORMAT(lastScalesDownloadTime, '%Y-%m-%d %H:%i:%S'), " +
                        "monitorCustomerDownload, " +
                        "DATE_FORMAT(lastCustomerDownloadTime, '%Y-%m-%d %H:%i:%S'), " +
                        "monitorPurchaseOrderDownload, " +
                        "DATE_FORMAT(lastPurchaseOrderDownloadTime, '%Y-%m-%d %H:%i:%S'), " +
                        "monitorSapMovementDownload, " +
                        "DATE_FORMAT(lastSapMovementDownloadTime, '%Y-%m-%d %H:%i:%S'), " +
                        "monitorVendorDownload, " +
                        "DATE_FORMAT(lastVendorDownloadTime, '%Y-%m-%d %H:%i:%S'), " +
                        "monitorShiftsNotClosed, " +
                        "numberOfShiftsNotClosed, " +
                        "monitorCloseShop, " +
                        "DATE_FORMAT(lastCloseShopTime, '%Y-%m-%d %H:%i:%S'), " +
                        "DATE_FORMAT(lastCloseShopTime, '%Y-%m-%d'), " +
                        "monitorBackup, " +
                        "backup, " +
                        "monitorDbReplication, " +
                        "dbReplicationStatus, " +
                        "monitorOpenStockTakes, " +
                        "numberOfOpenStockTakes, " +
                        "monitorPOsNotReceived, " +
                        "numberOfPOsNotReceived, " +
                        "numberOfPOsNotReceivedWithinDays, " +
                        "monitorIncompleteCustomerCreation, " +
                        "numberOfIncompleteCustomers " +
                        "FROM clientMonitorProcesses " +
                        "WHERE clientid = '"+clientid+"' " +
                        "AND site = '"+site+"' "+
                        "AND lastUpdate = " +
                        "( " +
                        "SELECT MAX(lastupdate) AS lastupdate " +
                        "FROM clientMonitorProcesses " +
                        "WHERE clientId =  'ArthurFord' " +
                        "AND site = '"+site+"'" +
                        ")" +
                        "ORDER BY 1 DESC LIMIT 0,1";                                    // only return the last record

            System.out.println("sqlString=" + sqlString);

            //localStatement = globalStuff.remoteDbConnector.createStatement();
            //dbresult = localStatement.executeQuery(sqlString);
            
            ResultSet dbresult = remoteStmt.executeQuery(sqlString);

            while (dbresult.next())
            {
                dbrows.add(dbresult.getString(1) + "|" + dbresult.getString(2) + "|" + dbresult.getString(3) + "|" + dbresult.getString(4) + "|" + dbresult.getString(5) + "|" +
                dbresult.getString(6) + "|" + dbresult.getString(7) + "|" + dbresult.getString(8) + "|" + dbresult.getString(9) + "|" + dbresult.getString(10) + "|" +
                dbresult.getString(11) + "|" + dbresult.getString(12) + "|" + dbresult.getString(13) + "|" + dbresult.getString(14) + "|" + dbresult.getString(15) + "|" +
                dbresult.getString(16) + "|" + dbresult.getString(17) + "|" + dbresult.getString(18) + "|" + dbresult.getString(19) + "|" + dbresult.getString(20) + "|" +
                dbresult.getString(21) + "|" + dbresult.getString(22) + "|" + dbresult.getString(23) + "|" + dbresult.getString(24) + "|" + dbresult.getString(25) + "|" +
                dbresult.getString(26) + "|" + dbresult.getString(27) + "|" + dbresult.getString(28) + "|" + dbresult.getString(29) + "|" + dbresult.getString(30) + "|" +
                dbresult.getString(31) + "|" + dbresult.getString(32) + "|" + dbresult.getString(33) + "|" + dbresult.getString(34) + "|" + dbresult.getString(35) + "|" +
                dbresult.getString(36) + "|" + dbresult.getString(37) + "|" + dbresult.getString(38) + "|" + dbresult.getString(39) + "|" + dbresult.getString(40) + "|" +
                dbresult.getString(41) + "|" + dbresult.getString(42) + "|" + dbresult.getString(43) + "|" + dbresult.getString(44) + "|" + dbresult.getString(45) + "|");
            }

        }
        catch (Exception ex)
        {
            System.err.println("pullClientclientMonitorProcessesForClientIdAndSite error - " + ex);
            cat.error("pullClientclientMonitorProcessesForClientIdAndSite error - " + ex);
            dynamicStaticAlerts.dynamicErrorPopUpCreation("Error:", "pullClientclientMonitorProcessesForClientIdAndSite - Error retrieving data.");
        }
        finally
        {
            close();
        }

        return dbrows;
    }

    // connect to the local sql database
    public boolean connectToLocalDb()
    {
        System.out.println("connectToLocalDb globalStuff.getLocalDbURL()="+globalStuff.getLocalDbURL()+" globalStuff.getLocalDbName()="+globalStuff.getLocalDbName());
        
        Boolean returnval = true;

        try
        {

            String url = "jdbc:mysql://" + globalStuff.getLocalDbURL() + "/" + globalStuff.getLocalDbName() + "";

            localDbConnector = DriverManager.getConnection(url, globalStuff.getLocalDbUser(), globalStuff.getLocalDbPw());

        }
        catch (Exception ex)
        {
            returnval = false;
            System.err.println("connectToLocalDb error - " + ex);
            cat.error("connectToLocalDb error - " + ex);
            //dynamicStaticAlerts.dynamicErrorPopUpCreation("SQLPull Error:", "connectToLocalDb - Error connecting to database. "+ex+" globalStuff.getDbURL()="+globalStuff.getDbURL()+" globalStuff.getDbName()="+globalStuff.getDbName());  
        }
        
        return returnval;
    }
    
    
    // connect to the remote sql database
    public boolean connectToRemoteDb()
    {
        System.out.println("connectToRemoteDb globalStuff.getRemoteDbURL()="+globalStuff.getRemoteDbURL()+" globalStuff.getRemoteDbName()="+globalStuff.getRemoteDbName());
        
        Boolean returnval = true;

        try
        {

            //String url = "jdbc:mysql://" + globalStuff.getRemoteDbURL() + "/" + globalStuff.getRemoteDbName() + "";
            //localDbConnector = DriverManager.getConnection(url, globalStuff.getRemoteDbUser(), globalStuff.getRemoteDbPw());

            // set up connection to remote database
            MysqlDataSource remoteDataSource = new MysqlDataSource();
            String url = "jdbc:mysql://"+globalStuff.getRemoteDbURL()+"/"+globalStuff.getRemoteDbName();
            remoteDataSource.setURL(url);
            remoteDataSource.setUser(globalStuff.getRemoteDbUser());
            remoteDataSource.setPassword(globalStuff.getRemoteDbPw());

            remoteConn = remoteDataSource.getConnection();
            remoteStmt = remoteConn.createStatement();

            System.out.println("Remote database "+url+" connected");

        }
        catch (Exception ex)
        {
            returnval = false;
            System.err.println("connectToRemoteDb error - " + ex);
            cat.error("connectToRemoteDb error - " + ex);
            //dynamicStaticAlerts.dynamicErrorPopUpCreation("SQLPull Error:", "connectToRemoteDb - Error connecting to database. "+ex+" globalStuff.getDbURL()="+globalStuff.getDbURL()+" globalStuff.getDbName()="+globalStuff.getDbName());  
        }
        
        return returnval;
    }

    // close open db connections
    public void close()
    {
        try
        {
            System.out.println("close");

            if (dbresult != null)
            {
                dbresult.close();
            }
            if (localStatement != null)
            {
                localStatement.close();
            }
            if (localDbConnector != null)
            {
                localDbConnector.close();
            }
            if (remoteStmt != null)
            {
                remoteStmt.close();
            }
            if (remoteConn != null)
            {
                remoteConn.close();
            }

        }
        catch (Exception e)
        {
            System.err.println("close exception" + e.getMessage());
            cat.error("close exception" + e.getMessage());

        }
    }
    
    
}
