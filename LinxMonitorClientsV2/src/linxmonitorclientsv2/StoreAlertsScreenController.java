/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package linxmonitorclientsv2;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Period;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;
import java.util.ResourceBundle;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Rectangle2D;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.stage.Screen;
import static linxmonitorclientsv2.LinxMonitorClientsV2.globalStuff;
import static linxmonitorclientsv2.LinxMonitorClientsV2.sqlPull;


/**
 * FXML Controller class
 *
 * @author marsi
 */
public class StoreAlertsScreenController implements Initializable
{
    private org.apache.log4j.Logger cat = org.apache.log4j.Logger.getLogger("StoreAlertsScreenController");

    @FXML public AnchorPane mainAnchorPane;
    @FXML public GridPane topGridPane;
    @FXML public GridPane storesGridPane;
    
    @FXML ImageView topImage;
    
    @FXML Label headerLabel1;
    @FXML Label headerLabel2;

    @FXML Label storeNameLabel1;
    @FXML Label storeNameLabel2;
    @FXML Label storeNameLabel3;
    @FXML Label storeNameLabel4;
    @FXML Label storeNameLabel5;
    @FXML Label storeNameLabel6;
    
    @FXML Label downloadTimeLabel1;
    @FXML Label downloadTimeLabel2;
    @FXML Label downloadTimeLabel3;
    @FXML Label downloadTimeLabel4;
    @FXML Label downloadTimeLabel5;
    @FXML Label downloadTimeLabel6;
    
    @FXML Label statusLabel1_1;
    @FXML Label statusLabel1_2;
    @FXML Label statusLabel1_3;
    @FXML Label statusLabel1_4;
    @FXML Label statusLabel1_5;
    @FXML Label statusLabel1_6;
    @FXML Label statusLabel1_7;
    
    @FXML Label statusLabel2_1;
    @FXML Label statusLabel2_2;
    @FXML Label statusLabel2_3;
    @FXML Label statusLabel2_4;
    @FXML Label statusLabel2_5;
    @FXML Label statusLabel2_6;
    @FXML Label statusLabel2_7;
    
    @FXML Label statusLabel3_1;
    @FXML Label statusLabel3_2;
    @FXML Label statusLabel3_3;
    @FXML Label statusLabel3_4;
    @FXML Label statusLabel3_5;
    @FXML Label statusLabel3_6;
    @FXML Label statusLabel3_7;
    
    @FXML Label statusLabel4_1;
    @FXML Label statusLabel4_2;
    @FXML Label statusLabel4_3;
    @FXML Label statusLabel4_4;
    @FXML Label statusLabel4_5;
    @FXML Label statusLabel4_6;
    @FXML Label statusLabel4_7;
    
    @FXML Label statusLabel5_1;
    @FXML Label statusLabel5_2;
    @FXML Label statusLabel5_3;
    @FXML Label statusLabel5_4;
    @FXML Label statusLabel5_5;
    @FXML Label statusLabel5_6;
    @FXML Label statusLabel5_7;
    
    @FXML Label statusLabel6_1;
    @FXML Label statusLabel6_2;
    @FXML Label statusLabel6_3;
    @FXML Label statusLabel6_4;
    @FXML Label statusLabel6_5;
    @FXML Label statusLabel6_6;
    @FXML Label statusLabel6_7;
    
    // screen layout variables
    double mainAnchorpaneWidth;
    double mainAnchorpaneHeight;
    double headerBoxHeight; 

    String normalLabelStyle = "-fx-text-fill: green;";
    String alertLabelStyle = "-fx-text-fill: red;";

    String[] sSplitArr;
    String[] sSplitArr2;
  
    DateTimeFormatter dateTimeFormat = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
    DateTimeFormatter dateFormat = DateTimeFormatter.ofPattern("yyyy-MM-dd");
    SimpleDateFormat sDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    SimpleDateFormat sDateTimeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    
    Integer lastScreenDisplayed = 0;        // used to control display of correct stores on screen        
    
    private LinxMonitorClientsV2 application;
    
    public void setApp(LinxMonitorClientsV2 application)
    {
        this.application = application;
    }
    
    private void DisplayScreen() 
    {
        System.out.println("DisplayScreen");

        HideAllFields();
        
        List<String> dbResults;
        List<String> dbResults2;
        
        // Display monitor screen for all clients
        if (globalStuff.getLinxasClientName().equals("All"))
        {
            dbResults = sqlPull.pullClientDatabases();
        }
        // display monitor screen for a specified client
        else
        {
            // Display all groups for the client
            if (globalStuff.getMonitorClientGroup().equals("All"))
            {
                dbResults = sqlPull.pullClientDatabasesForClientId(globalStuff.getLinxasClientName());
            }
            // Display a specified group for a client
            else
            {
                dbResults = sqlPull.pullClientDatabasesForClientIdAndMonitorGroup(globalStuff.getLinxasClientName(), globalStuff.getMonitorClientGroup());
            }
        }

        Iterator it = dbResults.iterator();

        String clientname = "";
        String monitorscreengroup = "";
        String shopname = "";
        String site = "";
        
        String clientgroup = "";
        String oldclientgroup = "";
        String headertext = "";

        String storelabelstyle = normalLabelStyle;
        String displaytimestyle = normalLabelStyle;
        
        Boolean success = false;
        Boolean foundRemotely = false;

        String storeName = "";
        String lastUpdate = "";
        String monitorJcoTransactionsInProcess = "";
        String jcoTransactionsInProcessCount = "";
        String jcoTransactionsLastDateProcessed = "";
        String monitorJcoTransactionsBatched = "";
        String jcoTransactionsBatchedCount = "";
        String jcoTransactionsLastDateBatched = "";
        String monitorJcoTransactionsNotProcessed = "";
        String jcoTransactionsNotProcessedCount = "";
        String jcoTransactionsLastDateNotProcessed = "";
        String monitorJcoTransactionsErrors = "";
        String jcoTransactionsErrorsCount = "";
        String monitorArticleDownload = "";
        String lastArticleDownloadTime = "";
        String monitorPriceListsDownload = "";
        String lastPriceListsDownloadTime = "";
        String monitorScalesDownload = "";
        String lastScalesDownloadTime = "";
        String monitorCustomerDownload = "";
        String lastCustomerDownloadTime = "";
        String monitorPurchaseOrderDownload = "";
        String lastPurchaseOrderDownloadTime = "";
        String monitorSapMovementDownload = "";
        String lastSapMovementDownloadTime = "";
        String monitorVendorDownload = "";
        String lastVendorDownloadTime = "";
        String monitorShiftsNotClosed = "";
        String numberOfShiftsNotClosed = "";
        String monitorCloseShop = "";
        String lastCloseShopTime = "";
        String lastCloseShopDate = "";
        String monitorBackup = "";
        String backup = "";
        String monitorDbReplication = "";
        String dbReplicationStatus = "";
        String monitorOpenStockTakes = "";
        String numberOfOpenStockTakes = "";
        String monitorPOsNotReceived = "";
        String numberOfPOsNotReceived = "";
        String numberOfPOsNotReceivedWithinDays = "";
        String monitorIncompleteCustomerCreation = "";
        String numberOfIncompleteCustomers = "";

        String statusLabelStyle1 = "";
        String statusLabelStyle2 = "";
        String statusLabelStyle3 = "";
        String statusLabelStyle4 = "";
        String statusLabelStyle5 = "";
        String statusLabelStyle6 = "";
        String statusLabelStyle7 = "";
        
        String statusLabelValue1 = "";
        String statusLabelValue2 = "";
        String statusLabelValue3 = "";
        String statusLabelValue4 = "";
        String statusLabelValue5 = "";
        String statusLabelValue6 = "";
        String statusLabelValue7 = "";
        
        Boolean showStatusLabel1 = false;
        Boolean showStatusLabel2 = false;
        Boolean showStatusLabel3 = false;
        Boolean showStatusLabel4 = false;
        Boolean showStatusLabel5 = false;
        Boolean showStatusLabel6 = false;
        Boolean showStatusLabel7 = false;
        
        LocalDateTime lastprocessdate = null;
        LocalDate lastdownloaddate = null;
        
        Integer notDownloadedCount = 0;
        
        Integer cnt = 0;
        Integer currentScreenDisplayed = 0;
        
        LocalDateTime timenow = null;
        LocalDate datenow = LocalDate.now();
        
        Boolean headerdisplayed = false;
        
        Calendar nowDate = Calendar.getInstance();
        String displayDateTime = sDateTimeFormat.format(nowDate.getTime());

        while (it.hasNext())   
        {
 
            cnt++;
            
            datenow = LocalDate.now();
            timenow = LocalDateTime.now();

            String value = (String) it.next();
            sSplitArr = value.split("\\|");
        
            clientname = sSplitArr[1];
            monitorscreengroup = sSplitArr[2];
            shopname = sSplitArr[3];
            site = sSplitArr[4];
            
            System.out.println("clientname="+clientname+" monitorscreengroup="+monitorscreengroup+" shopname="+shopname);
            
            clientgroup = clientname.toUpperCase().concat(monitorscreengroup.toUpperCase());
            
            System.out.println("lastScreenDisplayed="+lastScreenDisplayed);
            System.out.println("currentScreenDisplayed="+currentScreenDisplayed);
            System.out.println("cnt="+cnt);

            // display new screen
            if (!oldclientgroup.equals(clientgroup))
            {
                currentScreenDisplayed++;
                
                System.out.println("lastScreenDisplayed="+lastScreenDisplayed);
                System.out.println("currentScreenDisplayed="+currentScreenDisplayed);
                System.out.println("cnt="+cnt);
                
                oldclientgroup = clientgroup;
                
                // go to the next record if it should not be displayed on the screen
                if (currentScreenDisplayed <= lastScreenDisplayed)
                {
                    continue;
                }

                // completed screen display, so get out
                if (headerdisplayed)
                {
                    break;
                }

                oldclientgroup = clientgroup;
                HideAllFields();

                nowDate = Calendar.getInstance();
                displayDateTime = sDateTimeFormat.format(nowDate.getTime());
                
                displayTopLogo(clientname);
                headertext = clientname + " - " + monitorscreengroup;
                headerLabel1.setText(headertext);
                headerLabel2.setText(displayDateTime);

                cnt = 1;
                lastScreenDisplayed = currentScreenDisplayed;
                headerdisplayed = true;
            }
            else
            {
                if (currentScreenDisplayed < lastScreenDisplayed)
                {
                    continue;
                }
            }

            storeName = "";
            lastUpdate = "";
            monitorJcoTransactionsInProcess = "";
            jcoTransactionsInProcessCount = "";
            jcoTransactionsLastDateProcessed = "";
            monitorJcoTransactionsBatched = "";
            jcoTransactionsBatchedCount = "";
            jcoTransactionsLastDateBatched = "";
            monitorJcoTransactionsNotProcessed = "";
            jcoTransactionsNotProcessedCount = "";
            jcoTransactionsLastDateNotProcessed = "";
            monitorJcoTransactionsErrors = "";
            jcoTransactionsErrorsCount = "";
            monitorArticleDownload = "";
            lastArticleDownloadTime = "";
            monitorPriceListsDownload = "";
            lastPriceListsDownloadTime = "";
            monitorScalesDownload = "";
            lastScalesDownloadTime = "";
            monitorCustomerDownload = "";
            lastCustomerDownloadTime = "";
            monitorPurchaseOrderDownload = "";
            lastPurchaseOrderDownloadTime = "";
            monitorSapMovementDownload = "";
            lastSapMovementDownloadTime = "";
            monitorVendorDownload = "";
            lastVendorDownloadTime = "";
            monitorShiftsNotClosed = "";
            numberOfShiftsNotClosed = "";
            monitorCloseShop = "";
            lastCloseShopTime = "";
            lastCloseShopDate = "";
            monitorBackup = "";
            backup = "";
            monitorDbReplication = "";
            dbReplicationStatus = "";
            monitorOpenStockTakes = "";
            numberOfOpenStockTakes = "";
            monitorPOsNotReceived = "";
            numberOfPOsNotReceived = "";
            numberOfPOsNotReceivedWithinDays = "";
            monitorIncompleteCustomerCreation = "";
            numberOfIncompleteCustomers = "";
            foundRemotely = false;
            
            storelabelstyle = normalLabelStyle;
            displaytimestyle = normalLabelStyle;
            
            statusLabelStyle1 = normalLabelStyle;        
            statusLabelStyle2 = normalLabelStyle;
            statusLabelStyle3 = normalLabelStyle;
            statusLabelStyle4 = normalLabelStyle;
            statusLabelStyle5 = normalLabelStyle;
            statusLabelStyle6 = normalLabelStyle;
            statusLabelStyle7 = normalLabelStyle;

            statusLabelValue1 = "n/a";
            statusLabelValue2 = "n/a";
            statusLabelValue3 = "n/a";
            statusLabelValue4 = "n/a";
            statusLabelValue5 = "n/a";
            statusLabelValue6 = "n/a";
            statusLabelValue7 = "n/a";

            showStatusLabel1 = false;            // jco transactions not processed
            showStatusLabel2 = false;            // jco batched transactions 
            showStatusLabel3 = false;            // jco transaction errors
            showStatusLabel4 = false;            // downloads
            showStatusLabel5 = false;            // incomplete customer creation
            showStatusLabel6 = false;            // shifts not closed
            showStatusLabel7 = false;            // close shop
            
            notDownloadedCount = 0;
            
            // get the information for the store from the remote database
            dbResults2 = sqlPull.pullClientclientMonitorProcessesForClientIdAndSite(clientname, site);
            String value2 = "";
            
            Iterator it2 = dbResults2.iterator();
            if (it2.hasNext())   
            {

                System.out.println("Found entry in remote db for client "+clientname+" site "+site);
                
                value2 = (String) it2.next();
                sSplitArr2 = value2.split("\\|");

                storeName = sSplitArr2[2];
                lastUpdate = sSplitArr2[3];
                monitorJcoTransactionsInProcess = sSplitArr2[4];
                jcoTransactionsInProcessCount = sSplitArr2[5];
                jcoTransactionsLastDateProcessed = sSplitArr2[6];
                monitorJcoTransactionsBatched = sSplitArr2[7];
                jcoTransactionsBatchedCount = sSplitArr2[8];
                jcoTransactionsLastDateBatched = sSplitArr2[9];
                monitorJcoTransactionsNotProcessed = sSplitArr2[10];
                jcoTransactionsNotProcessedCount = sSplitArr2[11];
                jcoTransactionsLastDateNotProcessed = sSplitArr2[12];
                monitorJcoTransactionsErrors = sSplitArr2[13];
                jcoTransactionsErrorsCount = sSplitArr2[14];
                monitorArticleDownload = sSplitArr2[15];
                lastArticleDownloadTime = sSplitArr2[16];
                monitorPriceListsDownload = sSplitArr2[17];
                lastPriceListsDownloadTime = sSplitArr2[18];
                monitorScalesDownload = sSplitArr2[19];
                lastScalesDownloadTime = sSplitArr2[20];
                monitorCustomerDownload = sSplitArr2[21];
                lastCustomerDownloadTime = sSplitArr2[22];
                monitorPurchaseOrderDownload = sSplitArr2[23];
                lastPurchaseOrderDownloadTime = sSplitArr2[24];
                monitorSapMovementDownload = sSplitArr2[25];
                lastSapMovementDownloadTime = sSplitArr2[26];
                monitorVendorDownload = sSplitArr2[27];
                lastVendorDownloadTime = sSplitArr2[28];
                monitorShiftsNotClosed = sSplitArr2[29];
                numberOfShiftsNotClosed = sSplitArr2[30];
                monitorCloseShop = sSplitArr2[31];
                lastCloseShopTime = sSplitArr2[32];
                lastCloseShopDate = sSplitArr2[33];
                monitorBackup = sSplitArr2[34];
                backup = sSplitArr2[35];
                monitorDbReplication = sSplitArr2[36];
                dbReplicationStatus = sSplitArr2[37];
                monitorOpenStockTakes = sSplitArr2[38];
                numberOfOpenStockTakes = sSplitArr2[39];
                monitorPOsNotReceived = sSplitArr2[40];
                numberOfPOsNotReceived = sSplitArr2[41];
                numberOfPOsNotReceivedWithinDays = sSplitArr2[42];
                monitorIncompleteCustomerCreation = sSplitArr2[43];
                numberOfIncompleteCustomers = sSplitArr2[44];
                
                foundRemotely = true;

            }
            else
            {
                System.out.println("No entry in remote db for client "+clientname+" site "+site);
                storelabelstyle = alertLabelStyle;                         // display store name in red because no remote entry was found
                displaytimestyle = alertLabelStyle;
            }
            
            System.out.println("lastUpdate="+lastUpdate);
            
            if (lastUpdate != null && !lastUpdate.equals("null") && !lastUpdate.equals(""))
            {
                if (getMinutesBetweenDateAndNow(LocalDateTime.parse(lastUpdate, dateTimeFormat)) > 60)
                {
                    displaytimestyle = alertLabelStyle;
                }
            }
            else
            {
                displaytimestyle = alertLabelStyle;
            }

            /*if (monitorJcoTransactionsInProcess.equals("X"))
            {
                if (Integer.parseInt(jcoTransactionsInProcessCount) > 1)
                {
                    statusLabelStyle = alertLabelStyle;
                }

                // alert if jco has not been processed for some time
                lastprocessdate = LocalDateTime.parse(jcoTransactionsLastDateProcessed, dateTimeFormat);
                System.out.println("lastprocessdate="+lastprocessdate);
                if (lastprocessdate != null && !lastprocessdate.equals("null"))
                {
                    if (getMinutesBetweenDateAndNow(lastprocessdate) > 30)
                    {
                        statusLabelStyle = alertLabelStyle;
                    }
                }
                else
                {
                    statusLabelStyle = alertLabelStyle;
                }
            }*/

            if (monitorJcoTransactionsNotProcessed.equals("X"))
            {
                showStatusLabel1 = true;
                statusLabelValue1 = jcoTransactionsNotProcessedCount;
                if (Integer.parseInt(jcoTransactionsNotProcessedCount) > 10)
                {
                    statusLabelStyle1 = alertLabelStyle;
                }
            }
            
            if (monitorJcoTransactionsBatched.equals("X"))
            {
                showStatusLabel1 = true;
                statusLabelValue1 = jcoTransactionsBatchedCount;
                if (Integer.parseInt(jcoTransactionsBatchedCount) > 10)
                {
                    statusLabelStyle2 = alertLabelStyle;
                }
            }

            if (monitorJcoTransactionsErrors.equals("X"))
            {
                showStatusLabel3 = true;
                statusLabelValue3 = jcoTransactionsErrorsCount;
                if (Integer.parseInt(jcoTransactionsErrorsCount) > 0)
                {
                    statusLabelStyle3 = alertLabelStyle;
                }
            }

            if (monitorArticleDownload.equals("X"))
            {
                showStatusLabel4 = true;
                System.out.println("lastArticleDownloadTime="+lastArticleDownloadTime);
                // no articles downloaded today
                if (lastArticleDownloadTime != null && !lastArticleDownloadTime.equals("null") && !lastArticleDownloadTime.equals("0000-00-00 00:00:00"))
                {
                    lastdownloaddate= LocalDate.parse(lastArticleDownloadTime,dateTimeFormat);
                    System.out.println("lastdownloaddate="+lastdownloaddate);
                    System.out.println("datenow="+datenow);
                    System.out.println("LocalDate.parse(datenow.toString(),dateFormat)="+LocalDate.parse(datenow.toString(),dateFormat));
                    
                    if (!lastdownloaddate.equals(LocalDate.parse(datenow.toString(),dateFormat)))
                    {
                        statusLabelStyle4 = alertLabelStyle;
                        notDownloadedCount++;
                    }
                }
                else
                {
                    statusLabelStyle4 = alertLabelStyle;
                    notDownloadedCount++;
                }
            }

            if (monitorPriceListsDownload.equals("X"))
            {
                showStatusLabel3 = true;
                // no pricelists downloaded today
                if (lastPriceListsDownloadTime != null && !lastPriceListsDownloadTime.equals("null") && !lastPriceListsDownloadTime.equals("0000-00-00 00:00:00"))
                {
                    lastdownloaddate= LocalDate.parse(lastPriceListsDownloadTime,dateTimeFormat);
                    if (!lastdownloaddate.equals(LocalDate.parse(datenow.toString(),dateFormat)))
                    {
                        statusLabelStyle4 = alertLabelStyle;
                        notDownloadedCount++;
                    }
                }
                else
                {
                    statusLabelStyle4 = alertLabelStyle;
                    notDownloadedCount++;
                }
            }


            if (monitorScalesDownload.equals("X"))
            {
                showStatusLabel3 = true;
                // no scales downloaded today
                if (lastScalesDownloadTime != null && !lastScalesDownloadTime.equals("null") && !lastScalesDownloadTime.equals("0000-00-00 00:00:00"))
                {
                    lastdownloaddate= LocalDate.parse(lastScalesDownloadTime,dateTimeFormat);
                    if (!lastdownloaddate.equals(LocalDate.parse(datenow.toString(),dateFormat)))
                    {
                        statusLabelStyle4 = alertLabelStyle;
                        notDownloadedCount++;
                    }
                }
                else
                {
                    statusLabelStyle4 = alertLabelStyle;
                    notDownloadedCount++;
                }
            }

            if (monitorCustomerDownload.equals("X"))
            {
                showStatusLabel3 = true;
                // no customers downloaded today
                if (lastCustomerDownloadTime != null && !lastCustomerDownloadTime.equals("null") && !lastCustomerDownloadTime.equals("0000-00-00 00:00:00"))
                {
                    lastdownloaddate= LocalDate.parse(lastCustomerDownloadTime,dateTimeFormat);
                    if (!lastdownloaddate.equals(LocalDate.parse(datenow.toString(),dateFormat)))
                    {
                        statusLabelStyle4 = alertLabelStyle;
                        notDownloadedCount++;
                    }
                }
                else
                {
                    statusLabelStyle4 = alertLabelStyle;
                    notDownloadedCount++;
                }
            }

            if (monitorPurchaseOrderDownload.equals("X"))
            {
                showStatusLabel4 = true;
                // no po's downloaded today
                if (lastPurchaseOrderDownloadTime != null && !lastPurchaseOrderDownloadTime.equals("null") && !lastPurchaseOrderDownloadTime.equals("0000-00-00 00:00:00"))
                {
                    lastdownloaddate= LocalDate.parse(lastPurchaseOrderDownloadTime,dateTimeFormat);
                    if (!lastdownloaddate.equals(LocalDate.parse(datenow.toString(),dateFormat)))
                    {
                        statusLabelStyle4 = alertLabelStyle;
                        notDownloadedCount++;
                    }
                }
                else
                {
                    statusLabelStyle4 = alertLabelStyle;
                    notDownloadedCount++;
                }
            }

            if (monitorSapMovementDownload.equals("X"))
            {
                showStatusLabel4 = true;
                // no sap movement details downloaded today
                if (lastSapMovementDownloadTime != null && !lastSapMovementDownloadTime.equals("null") && !lastSapMovementDownloadTime.equals("0000-00-00 00:00:00"))
                {
                    lastdownloaddate= LocalDate.parse(lastSapMovementDownloadTime,dateTimeFormat);
                    if (!lastdownloaddate.equals(LocalDate.parse(datenow.toString(),dateFormat)))
                    {
                        statusLabelStyle4 = alertLabelStyle;
                        notDownloadedCount++;
                    }
                }
                else
                {
                    statusLabelStyle4 = alertLabelStyle;
                    notDownloadedCount++;
                }
            }

            if (monitorVendorDownload.equals("X"))
            {
                showStatusLabel4 = true;
                // no vendors downloaded today
                if (lastVendorDownloadTime != null && !lastVendorDownloadTime.equals("null") && !lastVendorDownloadTime.equals("0000-00-00 00:00:00"))
                {
                    lastdownloaddate= LocalDate.parse(lastVendorDownloadTime,dateTimeFormat);
                    if (!lastdownloaddate.equals(LocalDate.parse(datenow.toString(),dateFormat)))
                    {
                        statusLabelStyle4 = alertLabelStyle;
                        notDownloadedCount++;
                    }
                }
                else
                {
                    statusLabelStyle4 = alertLabelStyle;
                    notDownloadedCount++;
                }
            }
            
            // Download status should be displayed
            if (showStatusLabel4)
            {
                System.out.println("notDownloadedCount="+notDownloadedCount);
                statusLabelValue4 = notDownloadedCount.toString();    // number of old downloads (not completed today)
            }

            /*if (monitorBackup.equals("X"))
            {
                if (backup.equals("false"))
                {
                    statusLabelStyle = alertLabelStyle;
                }
            }*/
            
            /*if (monitorDbReplication.equals("X"))
            {
                if (!dbReplicationStatus.equals("RUNNING"))
                {
                    statusLabelStyle = alertLabelStyle;
                }
            }*/

            /*if (monitorOpenStockTakes.equals("X"))
            {
                showStatusLabel5 = true;
                statusLabelValue5 = numberOfOpenStockTakes;
                if (Integer.parseInt(numberOfOpenStockTakes) > 0)
                {
                    statusLabelStyle5 = alertLabelStyle;
                }
            }*/

            /*if (monitorPOsNotReceived.equals("X"))
            {
                showStatusLabel6 = true;
                statusLabelValue6 = numberOfPOsNotReceived;
                if (Integer.parseInt(numberOfPOsNotReceived) > 0)
                {
                    statusLabelStyle6 = alertLabelStyle;
                }
            }*/

            if (monitorIncompleteCustomerCreation.equals("X"))
            {
                showStatusLabel5 = true;
                statusLabelValue5 = numberOfIncompleteCustomers;
                if (Integer.parseInt(numberOfIncompleteCustomers) > 0)
                {
                    statusLabelStyle5 = alertLabelStyle;
                }
            }

            if (monitorShiftsNotClosed.equals("X"))
            {
                showStatusLabel6 = true;
                statusLabelValue6 = numberOfShiftsNotClosed;
                if (Integer.parseInt(numberOfShiftsNotClosed) > 0)
                {
                    statusLabelStyle6 = alertLabelStyle;
                }
            }
            
            if (monitorCloseShop.equals("X"))
            {
                showStatusLabel7 = true;
                statusLabelValue7 = lastCloseShopDate;
                // no close shop yesterday
                if (lastCloseShopTime != null && !lastCloseShopTime.equals("null") && !lastCloseShopTime.equals("0000-00-00 00:00:00"))
                {
                    lastdownloaddate= LocalDate.parse(lastCloseShopTime,dateTimeFormat);
                    Period intervalPeriod = Period.between(lastdownloaddate, datenow);
                    
                    System.out.println("lastdownloaddate="+lastdownloaddate);
                    System.out.println("datenow="+datenow);
                    System.out.println("closeshop difference in days="+intervalPeriod.getDays());
                    
                    if (intervalPeriod.getDays() > 1) 
                    {
                        statusLabelStyle7 = alertLabelStyle;
                    }
                }
                else
                {
                    statusLabelStyle7 = alertLabelStyle;
                }
            }

            // display red buttons for everything if the last download time is not valid
            if (displaytimestyle.equals(alertLabelStyle))
            {
                statusLabelStyle1 = alertLabelStyle;
                statusLabelStyle2 = alertLabelStyle;
                statusLabelStyle3 = alertLabelStyle;
                statusLabelStyle4 = alertLabelStyle;
                statusLabelStyle5 = alertLabelStyle; 
                statusLabelStyle6 = alertLabelStyle; 
                statusLabelStyle7 = alertLabelStyle;
            }

            if (cnt.equals(1))
            {
                storeNameLabel1.setVisible(true);
                storeNameLabel1.setText(shopname);
                storeNameLabel1.setStyle(storelabelstyle);

                downloadTimeLabel1.setVisible(true);
                downloadTimeLabel1.setText(lastUpdate);
                downloadTimeLabel1.setStyle(displaytimestyle);
                
                if (showStatusLabel1)
                {
                    statusLabel1_1.setVisible(true);
                    statusLabel1_1.setStyle(statusLabelStyle1);
                    statusLabel1_1.setText(statusLabelValue1);
                }
                if (showStatusLabel2)
                {
                    statusLabel1_2.setVisible(true);
                    statusLabel1_2.setStyle(statusLabelStyle2);
                    statusLabel1_2.setText(statusLabelValue2);
                }
                if (showStatusLabel3)
                {
                    statusLabel1_3.setVisible(true);
                    statusLabel1_3.setStyle(statusLabelStyle3);
                    statusLabel1_3.setText(statusLabelValue3);
                }
                if (showStatusLabel4)
                {
                    statusLabel1_4.setVisible(true);
                    statusLabel1_4.setStyle(statusLabelStyle4);
                    statusLabel1_4.setText(statusLabelValue4);
                }
                if (showStatusLabel5)
                {
                    statusLabel1_5.setVisible(true);
                    statusLabel1_5.setStyle(statusLabelStyle5);
                    statusLabel1_5.setText(statusLabelValue5);
                }
                if (showStatusLabel6)
                {
                    statusLabel1_6.setVisible(true);
                    statusLabel1_6.setStyle(statusLabelStyle6);
                    statusLabel1_6.setText(statusLabelValue6);
                }
                if (showStatusLabel7)
                {
                    statusLabel1_7.setVisible(true);
                    statusLabel1_7.setStyle(statusLabelStyle6);
                    statusLabel1_7.setText(statusLabelValue6);
                }
            }
            else if (cnt.equals(2))
            {
                storeNameLabel2.setVisible(true);
                storeNameLabel2.setText(shopname);
                storeNameLabel2.setStyle(storelabelstyle);

                downloadTimeLabel2.setVisible(true);
                downloadTimeLabel2.setText(lastUpdate);
                downloadTimeLabel2.setStyle(displaytimestyle);
                
                if (showStatusLabel1)
                {
                    statusLabel2_1.setVisible(true);
                    statusLabel2_1.setStyle(statusLabelStyle1);
                    statusLabel2_1.setText(statusLabelValue1);
                }
                if (showStatusLabel2)
                {
                    statusLabel2_2.setVisible(true);
                    statusLabel2_2.setStyle(statusLabelStyle2);
                    statusLabel2_2.setText(statusLabelValue2);
                }
                if (showStatusLabel3)
                {
                    statusLabel2_3.setVisible(true);
                    statusLabel2_3.setStyle(statusLabelStyle3);
                    statusLabel2_3.setText(statusLabelValue3);
                }
                if (showStatusLabel4)
                {
                    statusLabel2_4.setVisible(true);
                    statusLabel2_4.setStyle(statusLabelStyle4);
                    statusLabel2_4.setText(statusLabelValue4);
                }
                if (showStatusLabel5)
                {
                    statusLabel2_5.setVisible(true);
                    statusLabel2_5.setStyle(statusLabelStyle5);
                    statusLabel2_5.setText(statusLabelValue5);
                }
                if (showStatusLabel6)
                {
                    statusLabel2_6.setVisible(true);
                    statusLabel2_6.setStyle(statusLabelStyle6);
                    statusLabel2_6.setText(statusLabelValue6);
                }
                if (showStatusLabel7)
                {
                    statusLabel2_7.setVisible(true);
                    statusLabel2_7.setStyle(statusLabelStyle6);
                    statusLabel2_7.setText(statusLabelValue6);
                }
            }
            else if (cnt.equals(3))
            {
                storeNameLabel3.setVisible(true);
                storeNameLabel3.setText(shopname);
                storeNameLabel3.setStyle(storelabelstyle);

                downloadTimeLabel3.setVisible(true);
                downloadTimeLabel3.setText(lastUpdate);
                downloadTimeLabel3.setStyle(displaytimestyle);
                
                if (showStatusLabel1)
                {
                    statusLabel3_1.setVisible(true);
                    statusLabel3_1.setStyle(statusLabelStyle1);
                    statusLabel3_1.setText(statusLabelValue1);
                }
                if (showStatusLabel2)
                {
                    statusLabel3_2.setVisible(true);
                    statusLabel3_2.setStyle(statusLabelStyle2);
                    statusLabel3_2.setText(statusLabelValue2);
                }
                if (showStatusLabel3)
                {
                    statusLabel3_3.setVisible(true);
                    statusLabel3_3.setStyle(statusLabelStyle3);
                    statusLabel3_3.setText(statusLabelValue3);
                }
                if (showStatusLabel4)
                {
                    statusLabel3_4.setVisible(true);
                    statusLabel3_4.setStyle(statusLabelStyle4);
                    statusLabel3_4.setText(statusLabelValue4);
                }
                if (showStatusLabel5)
                {
                    statusLabel3_5.setVisible(true);
                    statusLabel3_5.setStyle(statusLabelStyle5);
                    statusLabel3_5.setText(statusLabelValue5);
                }
                if (showStatusLabel6)
                {
                    statusLabel3_6.setVisible(true);
                    statusLabel3_6.setStyle(statusLabelStyle6);
                    statusLabel3_6.setText(statusLabelValue6);
                }
                if (showStatusLabel7)
                {
                    statusLabel3_7.setVisible(true);
                    statusLabel3_7.setStyle(statusLabelStyle6);
                    statusLabel3_7.setText(statusLabelValue6);
                }
            }
            else if (cnt.equals(4))
            {
                storeNameLabel4.setVisible(true);
                storeNameLabel4.setText(shopname);
                storeNameLabel4.setStyle(storelabelstyle);

                downloadTimeLabel4.setVisible(true);
                downloadTimeLabel4.setText(lastUpdate);
                downloadTimeLabel4.setStyle(displaytimestyle);
                
                if (showStatusLabel1)
                {
                    statusLabel4_1.setVisible(true);
                    statusLabel4_1.setStyle(statusLabelStyle1);
                    statusLabel4_1.setText(statusLabelValue1);
                }
                if (showStatusLabel2)
                {
                    statusLabel4_2.setVisible(true);
                    statusLabel4_2.setStyle(statusLabelStyle2);
                    statusLabel4_2.setText(statusLabelValue2);
                }
                if (showStatusLabel3)
                {
                    statusLabel4_3.setVisible(true);
                    statusLabel4_3.setStyle(statusLabelStyle3);
                    statusLabel4_3.setText(statusLabelValue3);
                }
                if (showStatusLabel4)
                {
                    statusLabel4_4.setVisible(true);
                    statusLabel4_4.setStyle(statusLabelStyle4);
                    statusLabel4_4.setText(statusLabelValue4);
                }
                if (showStatusLabel5)
                {
                    statusLabel4_5.setVisible(true);
                    statusLabel4_5.setStyle(statusLabelStyle5);
                    statusLabel4_5.setText(statusLabelValue5);
                }
                if (showStatusLabel6)
                {
                    statusLabel4_6.setVisible(true);
                    statusLabel4_6.setStyle(statusLabelStyle6);
                    statusLabel4_6.setText(statusLabelValue6);
                }
                if (showStatusLabel7)
                {
                    statusLabel4_7.setVisible(true);
                    statusLabel4_7.setStyle(statusLabelStyle6);
                    statusLabel4_7.setText(statusLabelValue6);
                }
            }
            else if (cnt.equals(5))
            {
                storeNameLabel5.setVisible(true);
                storeNameLabel5.setText(shopname);
                storeNameLabel5.setStyle(storelabelstyle);

                downloadTimeLabel5.setVisible(true);
                downloadTimeLabel5.setText(lastUpdate);
                downloadTimeLabel5.setStyle(displaytimestyle);
                
                if (showStatusLabel1)
                {
                    statusLabel5_1.setVisible(true);
                    statusLabel5_1.setStyle(statusLabelStyle1);
                    statusLabel5_1.setText(statusLabelValue1);
                }
                if (showStatusLabel2)
                {
                    statusLabel5_2.setVisible(true);
                    statusLabel5_2.setStyle(statusLabelStyle2);
                    statusLabel5_2.setText(statusLabelValue2);
                }
                if (showStatusLabel3)
                {
                    statusLabel5_3.setVisible(true);
                    statusLabel5_3.setStyle(statusLabelStyle3);
                    statusLabel5_3.setText(statusLabelValue3);
                }
                if (showStatusLabel4)
                {
                    statusLabel5_4.setVisible(true);
                    statusLabel5_4.setStyle(statusLabelStyle4);
                    statusLabel5_4.setText(statusLabelValue4);
                }
                if (showStatusLabel5)
                {
                    statusLabel5_5.setVisible(true);
                    statusLabel5_5.setStyle(statusLabelStyle5);
                    statusLabel5_5.setText(statusLabelValue5);
                }
                if (showStatusLabel6)
                {
                    statusLabel5_6.setVisible(true);
                    statusLabel5_6.setStyle(statusLabelStyle6);
                    statusLabel5_6.setText(statusLabelValue6);
                }
                if (showStatusLabel7)
                {
                    statusLabel5_7.setVisible(true);
                    statusLabel5_7.setStyle(statusLabelStyle6);
                    statusLabel5_7.setText(statusLabelValue6);
                }
            }
            else if (cnt.equals(6))
            {
                storeNameLabel6.setVisible(true);
                storeNameLabel6.setText(shopname);
                storeNameLabel6.setStyle(storelabelstyle);

                downloadTimeLabel6.setVisible(true);
                downloadTimeLabel6.setText(lastUpdate);
                downloadTimeLabel6.setStyle(displaytimestyle);
                
                if (showStatusLabel1)
                {
                    statusLabel6_1.setVisible(true);
                    statusLabel6_1.setStyle(statusLabelStyle1);
                    statusLabel6_1.setText(statusLabelValue1);
                }
                if (showStatusLabel2)
                {
                    statusLabel6_2.setVisible(true);
                    statusLabel6_2.setStyle(statusLabelStyle2);
                    statusLabel6_2.setText(statusLabelValue2);
                }
                if (showStatusLabel3)
                {
                    statusLabel6_3.setVisible(true);
                    statusLabel6_3.setStyle(statusLabelStyle3);
                    statusLabel6_3.setText(statusLabelValue3);
                }
                if (showStatusLabel4)
                {
                    statusLabel6_4.setVisible(true);
                    statusLabel6_4.setStyle(statusLabelStyle4);
                    statusLabel6_4.setText(statusLabelValue4);
                }
                if (showStatusLabel5)
                {
                    statusLabel6_5.setVisible(true);
                    statusLabel6_5.setStyle(statusLabelStyle5);
                    statusLabel6_5.setText(statusLabelValue5);
                }
                if (showStatusLabel6)
                {
                    statusLabel6_6.setVisible(true);
                    statusLabel6_6.setStyle(statusLabelStyle6);
                    statusLabel6_6.setText(statusLabelValue6);
                }
                if (showStatusLabel7)
                {
                    statusLabel6_7.setVisible(true);
                    statusLabel6_7.setStyle(statusLabelStyle6);
                    statusLabel6_7.setText(statusLabelValue6);
                }
                cnt = 0;
            }
            
            // start next loop at the first screen
            if (!it.hasNext())
            {
                lastScreenDisplayed = 0;
            }
        }
    }
    
    private long getMinutesBetweenDateAndNow(LocalDateTime inputDate) 
    {
        System.out.println("getMinutesBetweenDateAndNow");

        LocalDateTime timenow = LocalDateTime.now();
        
        long timediff = inputDate.until(timenow, ChronoUnit.MINUTES);
        System.out.println("timediff="+timediff);

        return timediff;
    }                

    private void HideAllFields() 
    {
        System.out.println("HideAllFields");
        
        storeNameLabel1.setStyle(normalLabelStyle);
        storeNameLabel2.setStyle(normalLabelStyle);
        storeNameLabel3.setStyle(normalLabelStyle);
        storeNameLabel4.setStyle(normalLabelStyle);
        storeNameLabel5.setStyle(normalLabelStyle);
        storeNameLabel6.setStyle(normalLabelStyle);
        
        storeNameLabel1.setText("");
        storeNameLabel2. setText("");
        storeNameLabel3. setText("");
        storeNameLabel4. setText("");
        storeNameLabel5. setText("");
        storeNameLabel6. setText("");
        
        storeNameLabel1.setVisible(false);
        storeNameLabel2.setVisible(false);
        storeNameLabel3.setVisible(false);
        storeNameLabel4.setVisible(false);
        storeNameLabel5.setVisible(false);
        storeNameLabel6.setVisible(false);
        
        downloadTimeLabel1. setText("");
        downloadTimeLabel2. setText("");
        downloadTimeLabel3. setText("");
        downloadTimeLabel4. setText("");
        downloadTimeLabel5. setText("");
        downloadTimeLabel6. setText("");
        
        downloadTimeLabel1.setVisible(false);
        downloadTimeLabel2.setVisible(false);
        downloadTimeLabel3.setVisible(false);
        downloadTimeLabel4.setVisible(false);
        downloadTimeLabel5.setVisible(false);
        downloadTimeLabel6.setVisible(false);

        statusLabel1_1.setVisible(false);
        statusLabel1_2.setVisible(false);
        statusLabel1_3.setVisible(false);
        statusLabel1_4.setVisible(false);
        statusLabel1_5.setVisible(false);
        statusLabel1_6.setVisible(false);
        statusLabel1_7.setVisible(false);
        
        statusLabel2_1.setVisible(false);
        statusLabel2_2.setVisible(false);
        statusLabel2_3.setVisible(false);
        statusLabel2_4.setVisible(false);
        statusLabel2_5.setVisible(false);
        statusLabel2_6.setVisible(false);
        statusLabel2_7.setVisible(false);
        
        statusLabel3_1.setVisible(false);
        statusLabel3_2.setVisible(false);
        statusLabel3_3.setVisible(false);
        statusLabel3_4.setVisible(false);
        statusLabel3_5.setVisible(false);
        statusLabel3_6.setVisible(false);
        statusLabel3_7.setVisible(false);
        
        statusLabel4_1.setVisible(false);
        statusLabel4_2.setVisible(false);
        statusLabel4_3.setVisible(false);
        statusLabel4_4.setVisible(false);
        statusLabel4_5.setVisible(false);
        statusLabel4_6.setVisible(false);
        statusLabel4_7.setVisible(false);
        
        statusLabel5_1.setVisible(false);
        statusLabel5_2.setVisible(false);
        statusLabel5_3.setVisible(false);
        statusLabel5_4.setVisible(false);
        statusLabel5_5.setVisible(false);
        statusLabel5_6.setVisible(false);
        statusLabel5_7.setVisible(false);
        
        statusLabel6_1.setVisible(false);
        statusLabel6_2.setVisible(false);
        statusLabel6_3.setVisible(false);
        statusLabel6_4.setVisible(false);
        statusLabel6_5.setVisible(false);
        statusLabel6_6.setVisible(false);
        statusLabel6_7.setVisible(false);
        
    }

    public Boolean runSystemCommand(String command)
    {
        
        System.out.println("runSystemCommand "+command);

        try
        {
            Process p = Runtime.getRuntime().exec(command);
            BufferedReader inputStream = new BufferedReader(
                    new InputStreamReader(p.getInputStream()));

            String s = "";
            // reading output stream of the command
            while ((s = inputStream.readLine()) != null)
            {
                
                System.out.println(s);
                
                if (s.contains("Received = 0") || s.contains("100% loss") || s.contains("Request timed out") || s.contains("could not find"))
                {
                    return false;
                }
            }
            
            return true;

        }
        catch (Exception e)
        {
            System.out.println("runSystemCommand Error"+e);
            e.printStackTrace();
            return false;
        }
    }
    
    private boolean displayTopLogo(String clientname) 
    {
        System.out.println("displayTopLogo");
        
        Boolean returnval = true;
        
        String imageName = globalStuff.imagesDirectory + "BlankLogo.png";
        
        if (!clientname.equals("") && clientname != null)
        {
            imageName = globalStuff.imagesDirectory + clientname + "Logo.png";
        }
        System.out.println("imageName="+imageName);
        
        File file;
        Image image;
        
        try 
        {
            file = new File(imageName);
            image = new Image(file.toURI().toString()); 
        }
        catch (Exception ex)
        {
            imageName = globalStuff.imagesDirectory + "BlankLogo.png";
            file = new File(imageName);
            image = new Image(file.toURI().toString());
            returnval = false;
        }

        globalStuff.topImage = image;
        topImage.setImage(image);

        // get the image to fit on the screen 
        Double logoMaxWidth = 0.2 * mainAnchorpaneWidth;
        Double logoMaxHeight = headerBoxHeight;
        System.out.println("logoMaxWidth="+logoMaxWidth);
        System.out.println("logoMaxHeight="+logoMaxHeight);
        
        topImage.setFitHeight(logoMaxHeight);                    // set the image to the max height

        Double newLogoWidth = topImage.boundsInParentProperty().get().getWidth();  // is the image now too wide to fit?
        System.out.println("newLogoWidth="+newLogoWidth);
        if (newLogoWidth > logoMaxWidth)                                   // image is too wide to fit fit in the box       
        {
            topImage.setFitWidth(logoMaxWidth);                  // rather us the box width to display the image, should fit now
            System.out.println("changed width to "+logoMaxWidth);
        }
        
        return returnval;
        
    }  

    @Override
    public void initialize(URL url, ResourceBundle rb)
    {
	
        System.out.println("initialize StoreAlertsScreenController");
        //cat.info("initialize StoreAlertsScreenController");

        // screen layout
        Rectangle2D primScreenBounds = Screen.getPrimary().getVisualBounds();     
        mainAnchorpaneHeight = primScreenBounds.getHeight();
        mainAnchorpaneWidth = primScreenBounds.getWidth();
        
        headerBoxHeight = topImage.getFitHeight();
        System.out.println("mainAnchorpaneWidth ="+mainAnchorpaneWidth);
        System.out.println("mainAnchorpaneHeight ="+mainAnchorpaneHeight);         
        System.out.println("headerBoxHeight="+headerBoxHeight);
        
        mainAnchorpaneHeight = mainAnchorpaneHeight - 35;                       // to avoid footer being cut of at bottom of screen
        mainAnchorpaneWidth = mainAnchorpaneWidth - 40;                         // to avoid footer being cut of at side of screen
        mainAnchorPane.setPrefSize(mainAnchorpaneWidth, mainAnchorpaneHeight);  // screen size = full screen
        
        topGridPane.getColumnConstraints().get(0).setPrefWidth(mainAnchorpaneWidth);
        
        storesGridPane.getRowConstraints().get(0).setPrefHeight(0.7 * headerBoxHeight);
        storesGridPane.getRowConstraints().get(1).setPrefHeight(0.2 * headerBoxHeight);
        storesGridPane.getRowConstraints().get(2).setPrefHeight(0.1 * headerBoxHeight);

        storesGridPane.getColumnConstraints().get(0).setPrefWidth(0.01 * mainAnchorpaneWidth);
        storesGridPane.getColumnConstraints().get(1).setPrefWidth(0.18 * mainAnchorpaneWidth);
        storesGridPane.getColumnConstraints().get(2).setPrefWidth(0.105 * mainAnchorpaneWidth);
        storesGridPane.getColumnConstraints().get(3).setPrefWidth(0.105 * mainAnchorpaneWidth);
        storesGridPane.getColumnConstraints().get(4).setPrefWidth(0.105 * mainAnchorpaneWidth);
        storesGridPane.getColumnConstraints().get(5).setPrefWidth(0.105 * mainAnchorpaneWidth);
        storesGridPane.getColumnConstraints().get(6).setPrefWidth(0.105 * mainAnchorpaneWidth);
        storesGridPane.getColumnConstraints().get(7).setPrefWidth(0.105 * mainAnchorpaneWidth);
        storesGridPane.getColumnConstraints().get(8).setPrefWidth(0.18 * mainAnchorpaneWidth);

        
        Double rowheight = mainAnchorpaneHeight / 20;
      
        storesGridPane.getRowConstraints().get(0).setPrefHeight(rowheight*2);
        storesGridPane.getRowConstraints().get(1).setPrefHeight(rowheight);
        storesGridPane.getRowConstraints().get(2).setPrefHeight(rowheight);
        storesGridPane.getRowConstraints().get(3).setPrefHeight(rowheight);
        storesGridPane.getRowConstraints().get(4).setPrefHeight(rowheight);
        storesGridPane.getRowConstraints().get(5).setPrefHeight(rowheight);
        storesGridPane.getRowConstraints().get(6).setPrefHeight(rowheight);
        storesGridPane.getRowConstraints().get(7).setPrefHeight(rowheight);
        storesGridPane.getRowConstraints().get(8).setPrefHeight(rowheight);
        storesGridPane.getRowConstraints().get(9).setPrefHeight(rowheight);
        storesGridPane.getRowConstraints().get(10).setPrefHeight(rowheight);
        storesGridPane.getRowConstraints().get(11).setPrefHeight(rowheight);
        storesGridPane.getRowConstraints().get(12).setPrefHeight(rowheight);
        storesGridPane.getRowConstraints().get(13).setPrefHeight(rowheight);
        storesGridPane.getRowConstraints().get(14).setPrefHeight(rowheight);
        storesGridPane.getRowConstraints().get(15).setPrefHeight(rowheight);
        storesGridPane.getRowConstraints().get(16).setPrefHeight(rowheight);
        storesGridPane.getRowConstraints().get(17).setPrefHeight(rowheight);
        storesGridPane.getRowConstraints().get(18).setPrefHeight(rowheight);

        displayTopLogo("");
        
        headerLabel1.setText("Please wait - program starting");
        headerLabel2.setText("");

        DisplayScreen();

        
        // screen updates run on different thread
        Thread thread = new Thread(new Runnable() 
        {

            @Override
            public void run() 
            {
                Runnable updater = new Runnable() 
                {

                    @Override
                    public void run() 
                    {
                        DisplayScreen();
                    }
                };

                while (true) 
                {
                    try 
                    {
                        System.out.println("sleep zzzzzzzzzzzzzzz");
                        Thread.sleep(globalStuff.getTimeoutMilliseconds());
                    } 
                    catch (InterruptedException ex) 
                    {
                        System.err.println("Runnable thread InterruptedException "+ex);
                    }

                    // UI update is run on the Application thread
                    Platform.runLater(updater);
                }
            }

        });
        // don't let thread prevent JVM shutdown
        thread.setDaemon(true);
        thread.start();

    }        
}

// *************************************************************************************************************************************************
