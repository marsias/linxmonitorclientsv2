/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package linxmonitorclientsv2;

import java.util.Optional;
import javafx.scene.control.Alert;
import javafx.scene.control.TextInputDialog;

/**
 *
 * @author marsi
 */
public class dynamicStaticAlerts
{
    
    public static String dynamicTextFieldPopUpCreation(String title, String instructionText, String fieldLabel)
    {
        //System.out.println("dynamicTextFieldPopUpCreation");
        
        TextInputDialog dialog = new TextInputDialog("");
        dialog.setTitle(title);
        dialog.setHeaderText(instructionText);
        dialog.setContentText(fieldLabel);

        // Traditional way to get the response value.
        Optional<String> result = dialog.showAndWait();
        
        if (result.isPresent())
        {
            //System.out.println("Result: " + result.get());
            return result.get();
        }
        
        return "";
    }
    
    // Display a warning dialog
    public static String dynamicAlertPopUpCreation(String title, String alertText)
    {
        //System.out.println("dynamicAlertPopUpCreation");
        
        Alert alert = new Alert(Alert.AlertType.WARNING);
        alert.setTitle(title);
        alert.setHeaderText(null);
        alert.setContentText(alertText);

        alert.showAndWait();
        
        return "";
    }
    
    // Display an information dialog
    public static String dynamicInfoPopUpCreation(String title, String alertText)
    {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle(title);
        alert.setHeaderText(null);
        alert.setContentText(alertText);

        alert.showAndWait();
        
        return "";
    }
    
    // Display an error dialog
    public static String dynamicErrorPopUpCreation(String title, String alertText)
    {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle(title);
        alert.setHeaderText(null);
        alert.setContentText(alertText);

        alert.showAndWait();
        
        return "";
    }

    // Display a confirmation dialog
    public static Boolean dynamicConfirmationPopUpCreation(String title, String alertText)
    {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle(title);
        alert.setHeaderText(null);
        alert.setContentText(alertText);

        alert.showAndWait();
        
        //System.out.println("(alert.getResult()="+alert.getResult()+" alert.getResult().getText()="+alert.getResult().getText());
        
        if (alert.getResult().getText().equals("OK"))
        {
            return true;
        }
        else
        {
            return false;
        }

    }
    
    // Display an information dialog
    public static String dynamicInfoPopUpCreationNoWait(String title, String alertText)
    {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle(title);
        alert.setHeaderText(null);
        alert.setContentText(alertText);

        alert.show();
        
        return "";
    }
}

// *** EOF ***