Linx Monitor Clients - version 2:
=================================

Contains code to display a monitor screen showing the statuses of client stores that need to be monitored by LinxAS

The program gets the status information to display on the screen from an Afrihost database (linxaxcn_monitorclients)
The Afrihost database gets populated by the program "Client POS Monitor" running at the stores. The program should be in startup to 
run constantly at the stores.
The local dashboard schema contains table clientdatabase with configuration information that allows retrieving records from the 
hosted database and controls the display of stores on the screen.

To install you need the following:
1. Create a folder C:\LinxMonitorClientsV2
2. Copy folder images to C:\LinxMonitorClientsV2
3. Make sure that the images folder contains logos of all the stores that will be monitored by the program, as well as file BlankLogo.png 
(used as default file)
    - The file should be named xxxxLogo.png where xxx is the name of the client
4. Copy folder reports to C:\LinxMonitorClientsV2
5. Edit file LinxASSummaryReportA4.jar in C:\NetBeansProjects\Linx Monitor Clients Version 2\Reports. The file should contain the following 
fields seperated by "|"
    - Name of the client to monitor (must match name used to name the logo file, name in remote db and name in local config database, or 
    "All" to scroll through all clients)
    - Group to monitor (must match monitorscreengroup field in dashboard.clientdatabases table or "All" to scroll through all groups for a client)
    - Hostname where dashbpard table resides (e.g. localhost:3306)
    - Db schema name (dashboard)
    - Db usernamelocal
    - Db login pw
    - Remote host URL to connect to to get monitor info
    - Remote db schema name (justforkix.aserv.co.za)
    - Remote db user (linxaxcn_monitorclients)
    - Remote db pw (linxaxcn_afstore)
    - Time in milliseconds to pause before refreshing the screen with new monitor info      
6. The clientdatabases table in the dashboard schema contains the program's local configuration information. Import the file "dashboard 
clientdatabases table.sql" in the "DB Files" folder.
7. Edit the clientdatabases table with the correct configuration information. The following fields need to be populated:
    - clientid: This should be the same name as the name of client in file LinxASSummaryReportA4.jar and also in the hosted database (ArthurFord)
    - monitorscreengroup: This field controls the grouping of stores displayed on the monitor screen. All stores that should be displayed 
    on the same screen should have the same name and will also be displayed at the top of the screen. Leave as null if all stores for the 
    client should be displayed on the screen (max of 15 stores can displayed at the same time)
    - shopname: The name of the shop
    - site: The site number. The site number should match the site in the hosted database.
    - The rest of the fields can be left blank
8. Copy file LinxMonitorClientsV2.jar and the lib folder to C:\LinxMonitorClientsV2
9. Create a shortcut to LinxMonitorClientsV2.jar


Notes on the display screen:
- A screen can display up to 15 stores
- To configure the store display on the screens, edit file LinxASSummaryReportA4.jar and update table clientdatabases (see above)
- The store name will be displayed in RED on the screen when the connection to the hosted database is down or when no entries could be found in the hosted database
- The date will be displayed in red if the latest information from the store is older than an hour or doesn't exist
- The button will be red if one of the monitor options at the store raises a flag and needs attention
- The button will be green if nothing at the store needs attention 